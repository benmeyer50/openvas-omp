#!/usr/bin/perl -w
#
# Filename:     2_get_version
# Description:  Test <get_version/> call to OMP server
# Creator:      Winfried Neessen <wn@neessen.net>
#
# $Id$
#
# Last modified: [ 2013-06-11 16:04:53 ]

use Test::More tests => 5;
use lib ( 'blib/lib', 'lib/', 'lib/OpenVAS' );
use Data::Dumper;
use OpenVAS::OMP

## Run ext. tests or not? {{{
print "Do you want to run extended tests (this will require a OMP server) [Y/n] ";
my $runExt = <STDIN>;
chomp( $runExt );
$runExt = 'y' if $runExt eq '';
# }}}

## Ask for server information {{{
my ( $server, $port );
if( $runExt eq 'y' or $runExt eq 'Y' )
{
	print "Servername [localhost]: ";
	$server = <STDIN>;
	chomp( $server );
	$server = 'localhost' if $server eq '';

	print "Port [9390]: ";
	$port = <STDIN>;
	chomp( $port );
	$port = 9390 if $port eq '';
}
# }}}

## Execute API call (skip if user doesn't have internet connectivity) {{{
SKIP: {

	## Define skip condition
	skip( '// No server to test specified. Skipping extended tests.', 5 ) unless( $runExt eq 'y' or $runExt eq 'Y' );

	## If connectivity is given, we can close the FH now
	pass( 'Server information provided.' );

	## Create new OpenVAS::OMP object {{{
	my $omp = OpenVAS::OMP->new
	(
		host		=> $server,
		ssl_verify	=> 0,
	);
	# }}}
	
	## Check if object has been defined {{{
	ok( defined( $omp ), 'OpenVAS::OMP object successfully created.' );
	ok( $omp->isa( 'OpenVAS::OMP' ), 'Object is an OpenVAS::OMP object.' );
	# }}}
	
	my $version = $omp->getVersion;
	ok( defined( $version ), 'Server responded.' );
	ok( $version >= 0.1, 'Server replied with valid server version: ' . $version );
	
}
# }}}
